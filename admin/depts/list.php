<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$page_path = "/admin/depts/list.php";

$action = get('action');
$id = get('id');
$sql = null;

switch ($action) {
    case 'delete':
        $sql = "DELETE FROM `depts` WHERE `dept_id`='{$id}'";
        break;
}

if (isset($sql)) {
    $db->query($sql);
    redirect($page_path);
}

if ($_POST) {
    $qr = $db->query("INSERT INTO `depts`(`dept_name`) VALUES ('{$_POST['dept_name']}')");

    if ($qr) {
        setAlert('success', "เพิ่มแผนกหรืองานต่างๆ {$_POST['dept_name']} สำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถเพิ่มแผนกหรืองานต่างๆ {$_POST['dept_name']} ได้");
    }
    
    redirect($page_path);
}

$items = db_result("SELECT * FROM `depts`");

ob_start();
?>
<?= showAlert() ?>
<h3>เพิ่มแผนกหรืองานต่างๆ</h3>
<form method="post">
    <label for="dept_name">ชื่อแผนกหรืองานต่างๆ</label>
    <input type="text" name="dept_name" id="dept_name" required>
    <button type="submit">บันทึก</button>
</form>

<h3>รายการแผนกหรืองานต่างๆ</h3>
<table>
    <thead>
        <th>รหัส</th>
        <th>ชื่อแผนกหรืองานต่างๆ</th>
        <th>จัดการแผนกหรืองานต่างๆ</th>
    </thead>
    <tbody>
        <?php foreach ($items as $item) : ?>
            <tr>
                <td><?= $item['dept_id'] ?></td>
                <td><?= $item['dept_name'] ?></td>
                <td>
                    <a href="<?= url("/admin/depts/edit.php?id={$item['dept_id']}") ?>">
                        แก้ไข
                    </a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="?action=delete&id=<?= $item['dept_id'] ?>"
                        <?= clickConfirm("คุณต้องการลบแผนกหรืองานต่างๆ {$item['dept_name']} หรือไม่") ?>
                    >
                        ลบ
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$layout_page = ob_get_clean();
$page_name = 'จัดการแผนกหรืองานต่างๆ';
require ROOT . '/admin/layout.php'; 
