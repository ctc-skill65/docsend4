<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$list_path = "/admin/depts/list.php";

$id = get('id');

if ($_POST) {
    $qr = $db->query("UPDATE `depts` SET `dept_name`='{$_POST['dept_name']}' WHERE `dept_id`='{$id}'");

    if ($qr) {
        setAlert('success', "แก้ไขแผนกหรืองานต่างๆ {$_POST['dept_name']} สำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถแก้ไขแผนกหรืองานต่างๆ {$_POST['dept_name']} ได้");
    }
    
    redirect($list_path);
}

$data = db_row("SELECT * FROM `depts` WHERE `dept_id`='{$id}'");

ob_start();
?>
<?= showAlert() ?>
<form method="post">
    <label for="dept_name">ชื่อแผนกหรืองานต่างๆ</label>
    <input type="text" name="dept_name" id="dept_name" value="<?= $data['dept_name'] ?>" required>
    <button type="submit">บันทึก</button>
</form>

<?php
$layout_page = ob_get_clean();
$page_name = 'แก้ไขแผนกหรืองานต่างๆ';
require ROOT . '/admin/layout.php'; 
