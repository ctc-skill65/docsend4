<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$list_path = "/admin/doc-types/list.php";

$id = get('id');

if ($_POST) {
    $qr = $db->query("UPDATE `doc_types` SET `doc_type_name`='{$_POST['doc_type_name']}' WHERE `doc_type_id`='{$id}'");

    if ($qr) {
        setAlert('success', "แก้ไขประเภทเอกสาร {$_POST['doc_type_name']} สำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถแก้ไขประเภทเอกสาร {$_POST['doc_type_name']} ได้");
    }
    
    redirect($list_path);
}

$data = db_row("SELECT * FROM `doc_types` WHERE `doc_type_id`='{$id}'");

ob_start();
?>
<?= showAlert() ?>
<form method="post">
    <label for="doc_type_name">ชื่อประเภทเอกสาร</label>
    <input type="text" name="doc_type_name" id="doc_type_name" value="<?= $data['doc_type_name'] ?>" required>
    <button type="submit">บันทึก</button>
</form>

<?php
$layout_page = ob_get_clean();
$page_name = 'แก้ไขประเภทเอกสาร';
require ROOT . '/admin/layout.php'; 
