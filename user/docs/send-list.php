<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('user');

$items = db_result("SELECT 
`docs` .* ,
`doc_types`.`doc_type_name`,
`users`.`firstname`,
`users`.`lastname`,
`users`.`email`,
to_user.firstname AS to_firstname,
to_user.lastname AS to_lastname,
to_user.email AS to_email,
to_dept.dept_name AS to_dept_name
FROM `docs` 
LEFT JOIN `doc_types` ON `doc_types`.`doc_type_id`=`docs`.`doc_type_id`
LEFT JOIN `users` ON `users`.`user_id`=`docs`.`user_id`
LEFT JOIN `users` AS to_user ON to_user.user_id=`docs`.`to_user_id`
LEFT JOIN `depts` AS to_dept ON to_dept.dept_id=`docs`.`to_dept_id`
WHERE `docs`.`user_id`='{$user_id}'
");

ob_start();
?>
<?= showAlert() ?>
<table>
    <thead>
        <tr>
            <th>รหัส</th>
            <th>ชื่อเอกสาร</th>
            <th>ประเภทเอกสาร</th>
            <th>ประเภทการส่ง</th>
            <th>ส่งจาก</th>
            <th>ส่งถึง</th>
            <th>วันที่ส่ง</th>
            <th>สถานะการอ่าน</th>
            <th>จำนวนดาวน์โหลด</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($items as $item) : ?>
            <tr>
                <td><?= $item['doc_id'] ?></td>
                <td><?= $item['doc_name'] ?></td>
                <td><?= $item['doc_type_name'] ?></td>
                <td>
                    <?php
                    switch ($item['send_type']) {
                        case 'user':
                            echo 'ส่งถึงผู้ใช้งาน';
                            break;
                        
                        case 'dept':
                            echo 'ส่งถึงแผนกหรืองานต่างๆ';
                            break;
                    }
                    ?>
                </td>
                <td>
                    <?= $item['firstname'] . ' ' . $item['lastname'] ?> 
                    <?= !empty($item['email']) ? '(' . $item['email'] . ')' : null ?>
                </td>
                <td>
                    <?php
                    switch ($item['send_type']) {
                        case 'user':
                            echo $item['to_firstname'] . ' ' . $item['to_lastname'];
                            echo !empty($item['to_email']) ? '(' . $item['to_email'] . ')' : null;
                            break;
                        
                        case 'dept':
                            echo 'แผนกหรืองาน: ' . $item['to_dept_name'];
                            break;
                    }
                    ?>
                </td>
                <td><?= $item['send_time'] ?></td>
                <td>
                    <?php
                    switch ($item['read_status']) {
                        case '1':
                            echo 'อ่านแล้ว';
                            break;
                        
                        case '0':
                            echo 'ยังไม่อ่าน';
                            break;
                    }
                    ?>
                </td>
                <td><?= $item['download'] ?></td>
                <td>
                    <a href="<?= url("/guest/download.php?id={$item['doc_id']}") ?>" target="_blank" rel="noopener noreferrer">
                        <button>
                            ดาวน์โหลด
                        </button>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$layout_page = ob_get_clean();
$page_name = 'รายการเอกสารที่ผู้ใช้งานส่ง';
require ROOT . '/user/layout.php'; 
