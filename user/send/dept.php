<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('user');

$page_path = "/user/send/dept.php";

if ($_POST) {
    $doc_file = upload('doc_file', '/storage/docs');

    $qr = $db->query("INSERT INTO `docs`(
    `doc_name`, 
    `doc_file`, 
    `doc_type_id`, 
    `user_id`, 
    `send_type`,  
    `to_dept_id`, 
    `read_status`, 
    `download`, 
    `send_time`) VALUES (
    '{$_POST['doc_name']}',
    '{$doc_file}',
    '{$_POST['doc_type_id']}',
    '{$user_id}',
    'dept',
    '{$_POST['send_id']}',
    0,
    0,
    NOW())");

    if ($qr) {
        setAlert('success', "ส่งเอกสาร {$_POST['doc_name']} สำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถส่งเอกสาร {$_POST['doc_name']} ได้");
    }
    
    redirect($page_path);
}

$doc_types = db_result("SELECT * FROM `doc_types`");
$items = db_result("SELECT * FROM `depts`");

ob_start();
?>
<?= showAlert() ?>
<form method="post" enctype="multipart/form-data">
    <label for="doc_name">ชื่อเอกสาร</label>
    <input type="text" name="doc_name" id="doc_name" required>
    <br>

    <label for="doc_file">ไฟล์เอกสาร</label>
    <input type="file" name="doc_file" id="doc_file" required>
    <br>


    <label for="doc_type_id">ประเภทเอกสาร</label>
    <select name="doc_type_id" id="doc_type_id">
        <option value="" selected disabled>---- เลือก ----</option>
        <?php foreach ($doc_types as $item) : ?>
            <option value="<?= $item['doc_type_id'] ?>">
                <?= $item['doc_type_name'] ?>
            </option>
        <?php endforeach; ?>
    </select>
    <br>

    <label for="send_id">ส่งถึง</label>
    <select name="send_id" id="send_id">
        <option value="" selected disabled>---- เลือก ----</option>
        <?php foreach ($items as $item) : ?>
            <option value="<?= $item['dept_id'] ?>">
                <?= $item['dept_name'] ?>
            </option>
        <?php endforeach; ?>
    </select>
    <br>

    <button type="submit">ส่ง</button>
</form>
<?php
$layout_page = ob_get_clean();
$page_name = 'ส่งเอกสารให้แผนกหรืองานต่างๆ';
require ROOT . '/user/layout.php'; 
