<?php
ob_start();
?>
<h1><?= conf('app_name') ?></h1>
<hr>

<h1><?= isset($page_name) ? $page_name : null ?></h1>

<h4><?= $user['firstname'] . ' ' . $user['lastname'] ?> (ผู้ใช้งานระบบ)</h4>

<nav>
    <h3>เมนู</h3>
    <ul>
        <li><a href="<?= url('/user/index.php') ?>">หน้าหลัก</a></li>
        <li>ส่งเอกสาร
            <ul>
                <li><a href="<?= url('/user/send/user.php') ?>">ส่งเอกสารให้ผู้ใช้งานคนอื่น</a></li>
                <li><a href="<?= url('/user/send/dept.php') ?>">ส่งเอกสารให้แผนกหรืองานต่างๆ</a></li>
            </ul>
        </li>
        <li>เอกสาร
            <ul>
                <li><a href="<?= url('/user/docs/send-list.php') ?>">รายการเอกสารที่ผู้ใช้งานส่ง</a></li>
                <li><a href="<?= url('/user/docs/get-list.php') ?>">รายการเอกสารที่ผู้ใช้งานคนอื่นส่งให้ผู้ใช้งาน</a></li>
                <li><a href="<?= url('/user/docs/dept-list.php') ?>">รายการเอกสารแผนกหรืองานต่างๆที่ผู้ใช้งานอยู่</a></li>
            </ul>
        </li>
        <li>ข้อมูลส่วนตัว
            <ul>
                <li><a href="<?= url('/user/profile/edit.php') ?>">แก้ไขข้อมูลส่วนตัว</a></li>
                <li><a href="<?= url('/user/profile/edit-pass.php') ?>">แก้ไขรหัสผ่าน</a></li>
                <li><a href="<?= url('/auth/logout.php') ?>" <?= clickConfirm("คุณต้องการออกจากระบบหรือไม่") ?>>ออกจากระบบ</a></li>
            </ul>
        </li>
    </ul>
</nav>

<main>
    <?= isset($layout_page) ? $layout_page : null ?>
</main>
<?php
$layout_body = ob_get_clean();
require INC . '/base_layout.php'; 
