<?php
require_once __DIR__ . '/../boot.php';
checkLogin();

$error = 'Not Found';

$id = get('id');
if (empty($id)) {
    exit($error);
}

$doc = db_row("SELECT * FROM `docs` WHERE `doc_id`='$id'");
if (empty($doc)) {
    exit($error);
}

if ($doc['to_user_id'] === $user_id || $doc['to_dept_id'] === $user['dept_id']) {
    $db->query("UPDATE `docs` SET `read_status`=1 WHERE `doc_id`='{$id}'");
}

$db->query("UPDATE `docs` SET `download`=`download`+1 WHERE `doc_id`='{$id}'");

redirect($doc['doc_file']);

echo $error;
