<?php

$db = new mysqli(
    conf('db_host'),
    conf('db_user'),
    conf('db_password'),
    conf('db_name')
);

if ($db->connect_error) {
    exit($db->connect_error);
}

$db->set_charset(conf('db_charset'));

function db_result($sql) {
    global $db;

    $result = $db->query($sql);
    
    $data = [];
    while ($row = $result->fetch_assoc()) {
        $data[] = $row;
    }

    return $data;
}

function db_row($sql) {
    global $db;

    $result = $db->query($sql);

    return $result->fetch_assoc();
}
